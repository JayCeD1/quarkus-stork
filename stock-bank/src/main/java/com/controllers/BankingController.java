package com.controllers;

import com.service.InventoryRestService;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("banking")
public class BankingController {

    @RestClient
    InventoryRestService service;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String greeting(){
        return "Hello from Banking Server";
    }

    @GET
    @Path("inventory")
    @Produces(MediaType.TEXT_PLAIN)
    public Uni<String> inventory(){
        return service.greeting()
                .onFailure()
                .retry().atMost(2);
    }

    @POST
    @Path("inventory")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.TEXT_PLAIN)
    public Uni<String> personal(String name){
        return service.personal(name)
                .onFailure()
                .retry()
                .atMost(2);
    }
}
