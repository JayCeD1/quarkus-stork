package com.service;

import io.quarkus.runtime.StartupEvent;
import io.vertx.ext.consul.ConsulClientOptions;
import io.vertx.ext.consul.ServiceOptions;
import io.vertx.mutiny.core.Vertx;
import io.vertx.mutiny.ext.consul.ConsulClient;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.event.Observes;

public class Registration {
    @ConfigProperty(name = "consul.host") String host;

    @ConfigProperty(name = "consul.port") int port;

    @ConfigProperty(name = "bank-service-port", defaultValue = "8083") int bankServicePort;

    public void init(@Observes StartupEvent e, Vertx vertx){
        ConsulClient client = ConsulClient.create(vertx, new ConsulClientOptions().setHost(host).setPort(port));

        client.registerServiceAndAwait(
                new ServiceOptions().setPort(bankServicePort).setAddress("localhost").setName("testService").setId("banking"));

    }
}
