package com.service;

import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/")
@RegisterRestClient(baseUri = "stork://testService/inventory")
//@RegisterRestClient(baseUri = "http://localhost:8082/inventory")
public interface InventoryRestService {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    Uni<String> greeting();

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    Uni<String>personal(String name);
}
