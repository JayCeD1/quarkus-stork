package com.controllers;

import io.smallrye.mutiny.Uni;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("inventory")
public class InventoryController {


    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Uni<String> greeting(){
        return Uni.createFrom().item("Hello from Inventory Server Waah!!!");
    }

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Uni<String> personalised(String name){
        return Uni.createFrom().item("Hi "+name);
    }

}
